plugins {
    id("ios-app")
}

dependencies {
    sharedBitmaps(projects.sharedResources)
}
dependencyLocking {
    lockAllConfigurations()
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.sharedUi)
                implementation(projects.sharedLogic)
            }
        }
    }
}
